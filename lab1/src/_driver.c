#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/slab.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Lyschev, Bondarenko");
MODULE_DESCRIPTION("Count spaces in input string");
MODULE_VERSION("1.0");

static struct proc_dir_entry *proc_entry;

const size_t mem_size = 1024;
dev_t dev = 0;
static struct class *cl;
static struct cdev c_dev;
char *input_buffer;
char *result_str_buffer;

#define RESULTS_BUF_SIZE 5
int results[RESULTS_BUF_SIZE];
int result_sp = 0;

static ssize_t proc_read(struct file *file, char __user *buf, size_t count, loff_t *off) {
    int i, offset = 0;

    for (i = 0; i < RESULTS_BUF_SIZE; i++)
        if (results[i] != INT_MIN)
            offset += sprintf(result_str_buffer + offset, "result #num=%d #value=%d\n", i, results[i]);

    if (*off > 0 || count < offset)
        return 0;

    if (copy_to_user(buf, result_str_buffer, offset) != 0)
        return -EFAULT;

    *off = offset;
    return offset;
}

static struct proc_ops fops = {
        .proc_read = proc_read,
};



static int dev_open(struct inode *i, struct file *f) {
    printk(KERN_INFO "Driver: open()\n");
    return 0;
}

static int dev_release(struct inode *i, struct file *f) {
    printk(KERN_INFO "Driver: close()\n");
    return 0;
}

static ssize_t dev_read(struct file *f, char __user *buf, size_t len, loff_t *off) {
    printk(KERN_INFO "Driver: read()\n");
    int i;
    int offset = 0;

    for (i = 0; i < RESULTS_BUF_SIZE; i++)
        if (results[i] != INT_MIN)
            offset += sprintf(result_str_buffer + offset, "result #num=%d #value=%d\n", i, results[i]);

    if (*off > 0 || len < offset)
        return 0;

    if (copy_to_user(buf, result_str_buffer, offset) != 0)
        return -EFAULT;
    *off = offset;
    return offset;
}

static ssize_t dev_write(struct file *f, const char __user *buf, size_t len, loff_t *off) {
    printk(KERN_INFO "Driver: write()\n");
    char space = '\x20';
    char tab = '\x09';
    int sum = 0;

    if (simple_write_to_buffer(input_buffer, len, off, buf, len) < 0) {
    printk(KERN_ERR "Data Write : Err!\n");
    return -EFAULT;
    }
    int i = 0;
    while(input_buffer[i] != '\0' && input_buffer[i] != '\n') {
        if(input_buffer[i] == space || input_buffer[i] == tab) sum++;
        i++;
    }

    if (result_sp == RESULTS_BUF_SIZE) {
        int i;
        for (i = 0; i < RESULTS_BUF_SIZE - 1; i++) {
            results[i] = results[i + 1];
        }
        results[RESULTS_BUF_SIZE - 1] = sum;
        result_sp--;
    } else {
        results[result_sp++] = sum;
    }
    return len;
}

static struct file_operations devfops = {
        .owner      = THIS_MODULE,
        .read       = dev_read,
        .write      = dev_write,
        .open       = dev_open,
        .release    = dev_release,
};


static int __init proc_example_init(void) {
    int i;
    proc_entry = proc_create("var4", 0444, NULL, &fops);

    for (i = 0; i < RESULTS_BUF_SIZE; i++)
        results[i] = INT_MIN;

    if ((alloc_chrdev_region(&dev, 0, 1, "var4_Dev")) < 0) {
        printk(KERN_ERR "Cannot allocate major number\n");
        return -1;
    }
    printk(KERN_INFO "Major = %d Minor = %d \n", MAJOR(dev), MINOR(dev));
    cdev_init(&c_dev, &devfops);
    if ((input_buffer = (uint8_t *) kmalloc(mem_size, GFP_KERNEL)) == 0) {
        printk(KERN_INFO "Cannot allocate memory in kernel\n");
        goto r_inp_buff;
    }
    if ((result_str_buffer = (uint8_t *) kmalloc(mem_size, GFP_KERNEL)) == 0) {
        printk(KERN_INFO "Cannot allocate memory in kernel\n");
        goto r_str_buff;
    }
    if ((cdev_add(&c_dev, dev, 1)) < 0) {
        printk(KERN_ERR "Cannot add the device to the system\n");
        goto r_class;
    }
    if ((cl = class_create(THIS_MODULE, "var_class")) == NULL) {
        printk(KERN_ERR "Cannot create the struct class\n");
        goto r_class;
    }
    if ((device_create(cl, NULL, dev, NULL, "var4")) == NULL) {
        printk(KERN_ERR "Cannot create the Device 1\n");
        goto r_device;
    }

    return 0;

    r_device:
        class_destroy(cl);
    r_class:
        unregister_chrdev_region(dev, 1);
    r_inp_buff:
        kfree(input_buffer);
    r_str_buff:
        kfree(result_str_buffer);
    return -1;
}

static void __exit proc_example_exit(void) {
    proc_remove(proc_entry);
    device_destroy(cl, dev);
    class_destroy(cl);
    cdev_del(&c_dev);
    unregister_chrdev_region(dev, 1);
}

module_init(proc_example_init);
module_exit(proc_example_exit);
